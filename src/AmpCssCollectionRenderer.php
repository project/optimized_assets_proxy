<?php

namespace Drupal\optimized_assets_proxy;

use Drupal\Core\Asset\CssCollectionRenderer;
use Drupal\amp\Asset\AmpCssCollectionRenderer as BaseAmpCssCollectionRenderer;

/**
 * Wrapper around amp's AmpCssCollectionRendered that ensure needed css files are present on disk upfront.
 */
class AmpCssCollectionRenderer extends BaseAmpCssCollectionRenderer {

  /**
   * Overload render() to ensure the needed files are present.
   */
  public function render(array $css_assets) {
    // Do what the parent does: call the grandparent.
    $elements = CssCollectionRenderer::render($css_assets);
    if (!$this->ampService->isAmpRoute()) {
      return $elements;
    }

    foreach ($elements as $key => $element) {
      if ($element['#tag'] == 'style' && array_key_exists('#value', $element)) {
        $urls = preg_match_all('/@import url\("(.+)\?/', $element['#value'], $matches);
        $all_css = [];
        foreach ($matches[1] as $url) {
          //$css = file_get_contents(DRUPAL_ROOT . $url);
          if (!file_exists(DRUPAL_ROOT . $url)) {
            optimized_assets_proxy_restore_file($url, 'css');
          }
        }
      }
      elseif ($element['#tag'] == 'link' && array_key_exists('href', $element['#attributes'])) {
        $url = $element['#attributes']['href'];
        $provider = parse_url($url, PHP_URL_HOST);
        if (!empty($provider)) {
          // ...
        }
        else {
          // Strip any querystring off the url.
          if (strpos($url, '?') !== FALSE) {
            list($url, $query) = explode('?', $url);
          }
          //$css = file_get_contents(DRUPAL_ROOT . $url);
          if (!file_exists(DRUPAL_ROOT . $url)) {
            optimized_assets_proxy_restore_file($url, 'css');
          }
        }
      }
    }

    return parent::render($css_assets);
  }
}
