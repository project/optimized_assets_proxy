<?php

namespace Drupal\optimized_assets_proxy;

use Drupal\Core\Asset\AssetDumper as CoreAssetDumper;

class AssetDumper extends CoreAssetDumper {

  public function dump($data, $file_extension) {
      $uri = parent::dump($data, $file_extension);
      $connection = \Drupal::database();

    $connection
      ->insert('optimized_assets_proxy')
      ->fields([
        'file' => basename($uri),
        'data' => $data,
        'timestamp' => REQUEST_TIME,
      ])
      ->execute();

    return $uri;
  }
}
