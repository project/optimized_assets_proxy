<?php

namespace Drupal\optimized_assets_proxy\EventSubscriber;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MissingAsset implements EventSubscriberInterface {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   *
   */
  public function __construct(LoggerInterface $logger, FileSystemInterface $file_system, Connection $database, EventDispatcherInterface $event_dispatcher) {
    $this->logger = $logger;
    $this->fileSystem = $file_system;
    $this->database = $database;
    $this->eventDispatcher = $event_dispatcher;
  }

  public function findAndWrite(GetResponseEvent $event) {
    $request_path = $event->getRequest()->getPathInfo();

    //$assets_path = file_url_transform_relative(file_create_url('assets://'));
    // Check if core patch https://www.drupal.org/project/drupal/issues/3027639 is applied.
    if (in_array('assets', stream_get_wrappers())) {
      $stream_wrapper_uri = 'assets://';
    }
    else {
      $stream_wrapper_uri = 'public://';
    }
    $assets_path = file_url_transform_relative(file_create_url($stream_wrapper_uri));

    $matches = [];
    if (preg_match("@^$assets_path(css)/css_|(js)/js_@", $request_path, $matches)) {
      $asset_type = $matches[1];
      $uri = substr($request_path, 1);
      $data = optimized_assets_proxy_restore_file($uri, $asset_type);
      if ($data) {
        // Redirect to itself.
        $query = \Drupal::request()->query->all();
        $query_parameters = UrlHelper::filterQueryParameters($query);
        $location = Url::fromUri('base://' . $uri, [
          'query' => $query_parameters,
          'absolute' => TRUE,
        ])->toString();
        // Avoid redirection caching in upstream proxies.
        header("Cache-Control: must-revalidate, no-cache, post-check=0, pre-check=0, private");
        header("Location: $location");
        exit;
      }
    }
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    // Priority 240 is after ban middleware but before page cache.
    $events[KernelEvents::REQUEST][] = ['findAndWrite', 240];
    return $events;
  }
}
