<?php

namespace Drupal\optimized_assets_proxy;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Swap some css/js related services.
 */
class OptimizedAssetsProxyServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Replace core's asset dumper service with our own.
    $types = ['css', 'js'];
    foreach ($types as $type) {
      $definition = $container->getDefinition("asset.$type.dumper");
      $definition->setClass('\Drupal\optimized_assets_proxy\AssetDumper');
    }

    // Replace amp's css collection rendered with our wrapper.
    if ($container->hasDefinition('amp.css.collection_renderer')) {
      $definition = $container->getDefinition('amp.css.collection_renderer');
      $definition->setClass('\Drupal\optimized_assets_proxy\AmpCssCollectionRenderer');
    }
  }
}
